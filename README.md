# SHDH Playa del Carmen website

## Install

```
$ git clone git@github.com:kelevrium/shdhplaya.git
$ cd shdhplaya/
$ sudo npm install -g gulp
$ npm install && gulp init
$ node app
```

## Contributing

Fork this repo, install development dependencies with `$ npm install --dev` and send a pull request.

## License

```
DO WHAT THE FUCK YOU WANT TO + BEER/PIZZA PUBLIC LICENSE
Version 1, May 2011

Copyright (C) 2014 Contributors <https://github.com/ricardotun/shdhplaya/graphs/contributors>

DO WHAT THE FUCK YOU WANT TO + BEER/PIZZA PUBLIC LICENSE
TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

0. You just DO WHAT THE FUCK YOU WANT TO.
1. If you make a substantial amount of money by exercising clause 0,
   you should consider buying the author a beer or a pizza.
```