module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('User', {
    uid: DataTypes.INTEGER,
    email: DataTypes.STRING,
    name: DataTypes.STRING,
    activity: DataTypes.STRING,
    avatar: DataTypes.STRING,
    login_by: DataTypes.STRING, // twitter, facebook, github etc.
    profile: DataTypes.STRING,
  })
  return User
}