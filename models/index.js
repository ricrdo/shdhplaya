/* 
  Loads every file in models/ directory and call method associate whit db
*/
var fs        = require('fs')
  , path      = require('path')
  , Sequelize = require('sequelize')
  , lodash    = require('lodash')
  , db        = {}
  , sequelize = new Sequelize('shdh', 'root', null, {
    dialect: 'sqlite',
    storage: path.resolve(__dirname, '../shdh.sqlite'),
  });

// Reads the files on directory models/ and adds it to dict db
fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf('.') !== 0) && (file !== 'index.js')
  })
  .forEach(function(file) {
    var model = sequelize.import(path.join(__dirname, file))
    db[model.name] = model
  })
 
 // Call associate method on every models on models/ dir
Object.keys(db).forEach(function(modelName) {
  if ('associate' in db[modelName]) {
    db[modelName].associate(db)
  }
})

module.exports = lodash.extend({
  sequelize: sequelize,
  Sequelize: Sequelize
}, db)