var settings;
try {
  settings = require('./settings');
} catch(e) {
  try {
    settings = require(process.env.HOME+'/settings');
  } catch(e) {
    throw new Error('Could not load site config.')
  }
}

module.exports = settings;

/**
 * Module dependencies.
 */

var express = require('express');
var everyauth = require('everyauth');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');
var db = require('./models');
var Sequelize = require("sequelize");

var app = express();

everyauth.everymodule.findUserById( function (id, callback) {
  db.User.findById(id, function(err, user){
    callback(null, user);
  });
});

var auth = require('./auth.js')(app, settings);

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.static(path.join(__dirname, 'public')));

app.use(express.bodyParser())
app.use(express.cookieParser())
app.use(express.session({
  secret: settings.sessionSecret
}))

app.use(everyauth.middleware(app));

app.use(app.router);

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
  var edt = require('express-debug');
  edt(app, {
  });
}

// GET URLS
app.get('/', routes.index);
// POST URLS
app.post('/users', user.create);

// DB CONECCTION
db
  .sequelize
  .sync({ force: false })
  .complete(function(err) {
    if (err) {
      throw err
    } else {
        http.createServer(app).listen(app.get('port'), function(){
          console.log('Express server listening on port ' + app.get('port'));
        });
    }
  })
