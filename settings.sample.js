var settings = {

    'cookieSecret': '[secret-key]',
    'sessionSecret': '[secret-key]',

    'uri': 'http://localhost',

    'external': {

        'facebook': {
            appId: '[secret-key]',
            appSecret: '[secret-key]'
        },

        'twitter': {
            consumerKey: '[secret-key]',
            consumerSecret: '[secret-key]'
        },

        'github': {
            appId: '[secret-key]',
            appSecret: '[secret-key]'
        }

    }

    , 'debug': (process.env.NODE_ENV !== 'production')
};

module.exports = settings;