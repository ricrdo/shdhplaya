var db = require('../models')
var gravatar = require('gravatar');
var validator = require('validator');
var everyauth = require('everyauth');
/*
 * GET users listing.
 */

/* Escapes user input*/
 var escape_inputs = function(input){
    for (var field in input) {
      input[field] = validator.escape(input[field]);
    }
    return input
 }

exports.create = function(req, res){

    if (req.body.login_by == 'email') {
        var avatar = gravatar.url(req.body.email, {});
    } else {
        var avatar = req.body.avatar;
    }

    // Escape input
    var clean_data = escape_inputs(req.body);
    // findOrCreate to avoid duplicating user entries uid and email must match
    // or it will create another entry on the db
    db.User.findOrCreate(
        {
            profile: req.body.profile,
            email: req.body.email,
        },{
            avatar: avatar,
            name: req.body.name,
            activity: req.body.activity,
            login_by: req.body.login_by,
            uid: req.body.uid
        }
    ).success(function(user){
        if(!user.isNewRecord){
        // if the user is not a new one it allows it to overwrite their activity
            user.updateAttributes(
                {activity: req.body.activity}, ['activity']
            );
        }
        req.logout();
        res.redirect("/#registered");
    });
};