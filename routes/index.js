var db = require('../models')
/*
 * GET home page.
 */

exports.index = function(req, res){

    db.User.findAndCountAll().success(function(users){
        res.render('index',
        {
            title: 'Super Happy Dev House Playa del Carmen',
            req: req,
            res: res,
            users: users
        });
    });
};
