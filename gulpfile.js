var gulp = require('gulp');
var gutil = require('gulp-util');
var less = require('gulp-less');
var rename = require('gulp-rename');
var minifycss = require('gulp-minify-css');
var path = require('path');

gulp.task('build', function () {
  gulp.src('./source/less/style.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'source', 'less') ]
    }))
    .pipe(gulp.dest('./public/stylesheets'))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('./public/stylesheets'));
});

gulp.task('watch', function(){
    gulp.watch('./source/less/*.less', ['build']);
});

gulp.task('default', function() {
    gulp.start('init');
});

gulp.task('init', function(){
    gulp.src('./settings.sample.js')
        .pipe(rename('settings.js'))
        .pipe(gulp.dest('./'));
    gulp.start('build');
});
