var everyauth = require('everyauth');
var db = require('./models');

module.exports = function Server(expressInstance, settings) {

    everyauth.debug = settings.debug;

    everyauth.everymodule.handleLogout( function (req, res) {
        delete req.session.user;
        req.logout();
        res.writeHead(303, { 'Location': this.logoutRedirectPath() });
        res.end();
    });

    // Facebook
    if (settings.external && settings.external.facebook) {
        everyauth.facebook
        .appId(settings.external.facebook.appId)
        .appSecret(settings.external.facebook.appSecret)
        .myHostname(settings.uri)
        .scope('email')
        .findOrCreateUser(function (session, accessToken, accessTokenExtra, facebookUserMetaData) {
            console.log(facebookUserMetaData);
            return true;
    })
        .redirectPath('/#rsvp');
    }

    // Twitter
    if (settings.external && settings.external.twitter) {
        everyauth.twitter
        .myHostname(settings.uri)
        .consumerKey(settings.external.twitter.consumerKey)
        .consumerSecret(settings.external.twitter.consumerSecret)
        .findOrCreateUser(function (session, accessToken, accessSecret, twitterUser) {
            console.log(twitterUser);
            return true;
    })
        .redirectPath('/#rsvp');
    }

    // Github
    if (settings.external && settings.external.github) {
        everyauth.github
        .myHostname(settings.uri)
        .appId(settings.external.github.appId)
        .appSecret(settings.external.github.appSecret)
        .findOrCreateUser(function (session, accessToken, accessTokenExtra, githubUser) {
            console.log(githubUser);
            return true;
        })
        .redirectPath('/#rsvp');
    }

};
