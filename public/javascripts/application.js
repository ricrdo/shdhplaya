$(function(){

    map = new GMaps({
      div: '#gmap',
      lat: 20.636577,
      lng: -87.066702,
      scrollwheel: false,
      draggable: false
    });

    map.addMarker({
        lat: 20.636577,
        lng: -87.066702,
        infoWindow: {
            content: '<img class="img-rounded center-block" src="/images/nest.png"><br><p class="text-center"><b>En un coworking no sólo se comparte una oficina productivas y equipos, también ideas, experiencias y puntos de vista. Únete a la comunidad de Nest y trabaja en un ambiente tranquilo y profesional.</b></p><p class="text-center">Av. 10 entre calle 38 y 40 norte, 77720 Playa del Carmen, Quintana Roo</p>'
        }
    });

});
